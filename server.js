const http = require('http')
const port = 3000
const pid = process.pid
const usages = []

const hastyParser = req => {
  let body = ''
  req.on('data', chunk => body += chunk.toString())
  req.on('end', () => usages.push(JSON.parse(body)))
  return usages.length
}

http.createServer((req, res) => {
  try {
    if (req.headers['content-type'] === 'application/json') {
      res.end(`Stored usage count: ${hastyParser(req)}`)
    } else {
      res.end(400)
    }
  } catch (err) {
    console.error(err)
    res.end(500)
  }
}).listen(port, () => {
  console.log(`Server process: ${pid} listening on: ${port}`)
})
